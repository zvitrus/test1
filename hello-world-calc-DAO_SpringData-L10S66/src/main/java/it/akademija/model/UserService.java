package it.akademija.model;

import it.akademija.dao.UserDao;
import it.akademija.dao.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.jws.soap.SOAPBinding;
import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;


    @Transactional(readOnly = true)
    public List<User> getUsers(){
        return userRepository.findAll();
    }
    @Transactional
    public void createUser(CreateUserCommand cmd){
        User user = new User();
        user.setUsername(cmd.getUsername());
        user.setFirstName(cmd.getFirstName());
        user.setLastName(cmd.getLastName());
        user.setEmail(cmd.getEmail());
        userRepository.save(user);
    }
    @Transactional(readOnly = true)
    public User getUserByEmail(String email){
        return userRepository.findByEmail(email);
    }
    @Transactional
    public void removeUser(String username){
        userRepository.deleteByUsername(username);
    }
    @Transactional
    public User getUserByFirstNameAndLastName(String firstName, String lastName){
        return userRepository.findByFirstNameAndLastName(firstName, lastName);
    }

    public List<User> getUsersByPartOfEmail(String partOfEmail) {
       return  userRepository.findUserByEmailContaining(partOfEmail);
    }


//
//
//    @Autowired @Qualifier("repoUserDao")
//    private UserDao userDao;
////
//
//    @Transactional(readOnly = true)
//    public List<User> getUsers(){
//        return userDao.getUsers();
//    }
//    @Transactional
//    public void createUser(User user){
//        userDao.createUser(user);
//    }

}
