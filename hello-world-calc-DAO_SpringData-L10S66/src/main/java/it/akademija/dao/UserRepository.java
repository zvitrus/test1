package it.akademija.dao;

import it.akademija.model.User;
import org.apache.catalina.LifecycleState;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

     User findByEmail(String email);
     List<User> findUserByEmailContaining (String partOfEmail);
     void deleteByUsername(String username);
     User findByFirstNameAndLastName(String firstName, String lastName);




}
